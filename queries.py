
ArtistsHits = """
SELECT tracks.Composer, (tracks.UnitPrice * invoice_items.Quantity) as Summ
FROM tracks
INNER JOIN invoice_items ON tracks.TrackId = invoice_items.TrackId
GROUP BY tracks.Composer
ORDER BY Summ DESC
"""

AddLimitQuery = """
LIMIT ?
"""