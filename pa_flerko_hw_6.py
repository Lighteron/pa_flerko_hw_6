# 1. Вывести общую длительность треков в таблице в секундах, сгруппированную по музыкальным жанрам
# def get_genre_durations() -> /genres_durations
#
# 2. Вывести count самых продаваемых треков.
# count - необязательный параметр, ограничивает кол-во выводимых записей.
# Если не указан, то вернуть все. Показывать поля: названия композиций, сумма и кол-во проданных копий.
# def get_greatest_hits() -> /greatest_hits?count=20
#
# 3*. Вывести count самых продаваемых исполнителей. count - необязательный параметр, если не указан, то вернуть все.
# def get_greatest_artists() -> /greatest_artists?count=20
#
# 4*. В каком городе больше всего слушают Hip-Hop?
# Необязательный параметр count ограничивает кол-во возвращаемых городов.
# def get_stats_by_city() -> /stats_by_city?genre=Hip Hop/Rap&count=1
#
# 5. Создать в классе Circle (x, y, radius) булевый метод contains, который принимает в качестве параметра
# точку (экземпляр класса Point (x, y)) и проверяет находится ли данная точка внутри окружности.
# Координаты центра окружности и точки могут быть произвольными.
# Если точка попадает на окружность, то это считается вхождением.
# p = Point(1, 42)
# c = Circle(0, 0, 10)
# c.contains(p) # False
#
# 6*. Создать класс Robot и его потомков - классы SpotMini, Atlas и Handle.
# В родительском классе должно быть определен конструктор и как минимум 3 атрибута и 1 метод.
# В классах-потомках должны быть добавлены минимум по 1 новому методу и по 1 новому атрибуту.

from flask import Flask, jsonify
from queries import ArtistsHits, AddLimitQuery
from utils import execute_query, format_records, c_limit
from webargs.flaskparser import use_kwargs


app = Flask(__name__)


@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


# Task 1
@app.route("/genres_durations")
def get_genre_durations():
    query = "SELECT ( SUM ( tracks.Milliseconds ) / 1000 ), genres.Name " \
              "FROM tracks " \
              "INNER JOIN genres ON tracks.GenreId=genres.GenreId " \
              "GROUP BY tracks.GenreId"
    data = execute_query(query)
    res = format_records(data)
    return res


# Task 2
@app.route("/greatest_hits")
@use_kwargs(c_limit, location='query')
def get_greatest_hits(count: int):
    if not count:
        res = execute_query(ArtistsHits)
        return format_records(res)
    else:
        res = execute_query(ArtistsHits + AddLimitQuery, (count,))
        return format_records(res)


app.run(debug=True)


# Task 5
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Circle:
    def __init__(self, x, y, radius):
        self.x = x
        self.y = y
        self.r = radius

    def contains(self, p: Point):  # (x - x0)^2 + (y - y0)^2 <= R^2
        res = (p.x - self.x) ** 2 + (p.y - self.y) ** 2 <= self.r ** 2
        return res
