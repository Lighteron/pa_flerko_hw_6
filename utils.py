import sqlite3
from webargs import fields

c_limit = {
    "count": fields.Int(
        required=False,
        missing=None
    )
}


def execute_query(query, args=()):
    with sqlite3.connect('../pa_flerko_hw_6/chinook.db') as conn:
        cur = conn.cursor()
        cur.execute(query, args)
        conn.commit()
        records = cur.fetchall()
    return records


def format_records(records):
    return "<br>".join(str(rec) for rec in records)
